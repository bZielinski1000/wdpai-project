<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('registration', 'DefaultController');
Router::get('category', 'DefaultController');
Router::get('transaction', 'TransactionsController');
Router::get('budget', 'DefaultController');
Router::get('summary', 'DefaultController');
Router::get('loginView', 'LoginController');
Router::get('getTransactions', 'TransactionsController');
Router::get('getBudgets', 'BudgetController');

Router::post('addBudget', 'BudgetController');
Router::post('login', 'LoginController');
Router::post('logout', 'LoginController');
Router::post('register', 'LoginController');
Router::post('addCategory', 'CategoryController');
Router::post('getCategories', 'CategoryController');
Router::post('addTransaction', 'TransactionsController');

Router::delete('deleteTransaction', 'TransactionsController');
Router::delete('deleteCategory', 'CategoryController');
Router::delete('deleteBudget', 'BudgetController');

Router::run($path);