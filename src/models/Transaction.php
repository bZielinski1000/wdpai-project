<?php


class Transaction
{
    private $id;
    private $categoryId;
    private $name;
    private $value;
    private $date;

    public function __construct($id, $categoryId, $name, $value, $date)
    {
        $this->id = $id;
        $this->categoryId = $categoryId;
        $this->name = $name;
        $this->value = $value;
        $this->date = $date;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function getName()
    {
        return $this->name;
    }


    public function getValue()
    {
        if($this->value >= 0){
            $this->value = "+" . $this->value;
        }

        $array = explode('.',$this->value);
        if(sizeof($array) == 1){
            return $this->value . '.00';
        }else if(strlen($array[1]) == 1){
            return $this->value . '0';
        }
        return $this->value;
    }


    public function getDate()
    {
        return $this->date;
    }


    public function getFormattedDate(){
        $array = explode("-",$this->date);
        $jd = gregoriantojd($array[1],$array[2],$array[0]);

        $day = jddayofweek($jd,1);
        $month = jdmonthname($jd,1);

        return $day . ', ' . $month .' '. $array[0];
    }

    public function getDay(){
        $array = explode("-",$this->date);
        return $array[2];
    }

}