<?php


class budget
{
    private $id;
    private $userId;
    private $categoryTypeId;
    private $value;
    private $amountSpent;
    private $startDate;
    private $endDate;

    public function __construct($id, $userId, $categoryTypeId, $value, $amountSpent, $startDate, $endDate)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->categoryTypeId = $categoryTypeId;
        $this->value = $value;
        $this->amountSpent = $amountSpent;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getUserId()
    {
        return $this->userId;
    }

    public function getCategoryTypeId()
    {
        return $this->categoryTypeId;
    }


    public function getValue()
    {
        return $this->value;
    }


    public function getAmountSpent()
    {
        return $this->amountSpent;
    }


    public function getStartDate()
    {
        return $this->startDate;
    }


    public function getEndDate()
    {
        return $this->endDate;
    }




}