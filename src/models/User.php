<?php


class User
{
    private $id;
    private $email;
    private $password;
    private $name;
    private $permission;

    public function __construct($id, $email, $password, $name, $permission)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->permission = $permission;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function getName()
    {
        return $this->name;
    }


    public function getPermission()
    {
        return $this->permission;
    }


    public function getId()
    {
        return $this->id;
    }

}