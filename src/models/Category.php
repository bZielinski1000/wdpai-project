<?php


class Category
{
    private $id;
    private $name;
    private $icon;
    private $color;
    private $date;
    private $value;

    public function __construct($id, $name, $icon, $color, $date, $value)
    {
        $this->id = $id;
        $this->name = $name;
        $this->icon = $icon;
        $this->color = $color;
        $this->date = $date;

        if($value == null){
            $this->value = 0.00;
        }else{
            $this->value = $value;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getValue()
    {
        if($this->value >= 0){
            $this->value = "+" . $this->value;
        }

        $array = explode('.',$this->value);
        if(sizeof($array) == 1){
            return $this->value . '.00';
        }else if(strlen($array[1]) == 1){
            return $this->value . '0';
        }
        return $this->value;
    }

    public function isValuePositive(){
        if($this->value >= 0){
            return "plus";
        }else{
            return "minus";
        }
    }

    public function getMinDate(){
        $array = explode("-",$this->date);

        return  $array[0]."-".$array[1].'-01';
    }

    public function getMaxDate(){
        $array = explode("-",$this->date);

        $day = cal_days_in_month(CAL_GREGORIAN,$array[1],$array[0]);
        return  $array[0]."-".$array[1].'-'.$day;
    }


}