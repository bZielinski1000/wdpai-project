<?php
spl_autoload_register('autoLoader');

function autoLoader($className){
    $path = "src/controllers/";
    $extension = ".php";
    $fullPath = $path . $className . $extension;

    if(!file_exists($fullPath)){
        return false;
    }

    require_once $fullPath;
}