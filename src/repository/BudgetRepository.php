<?php
require_once 'Repository.php';
require_once __DIR__ . '/../models/Budget.php';

class BudgetRepository extends Repository
{
    function insertBudget($userId, $categoryTypeId, $value, $startDate, $endDate)
    {
        $conn = $this->database->connect();
        $stmt = $conn->prepare(
            'INSERT INTO public.user_budgets(user_id,category_type_id,"value",start_date,end_date) VALUES (?,?,?,?,?)'
        );
        $stmt->execute([$userId, $categoryTypeId, $value, $startDate, $endDate]);

        $budgetId = $conn->lastInsertId();

        $stmt = $conn->prepare(
            'SELECT ct.*,amount_spent FROM user_budgets as ub LEFT JOIN categories_types ct on ub.category_type_id = ct.id LEFT JOIN view_budgets_amount_spent vbas on ub.id = vbas.budget_id  WHERE ub.id = :budgetId'
        );
        $stmt->execute([$budgetId]);
        $budgetDetails = $stmt->fetch(PDO::FETCH_ASSOC);

        if($budgetDetails['amount_spent'] == null){
            $budgetDetails['amount_spent'] = 0;
        }

        return array(
            'budgetId' => $budgetId,
            'userId' => $userId,
            'categoryTypeId' => $categoryTypeId,
            'value' => $value,
            'amountSpent' => $budgetDetails['amount_spent'],
            'startDate' =>  $startDate,
            'endDate' => $endDate,
            'name' => $budgetDetails['name'],
            'icon' => $budgetDetails['icon'],
            'color' => $budgetDetails['color'],
            'permission' => true
        );
    }

    function getBudgets($userId){
        $conn = $this->database->connect();
        $stmt = $conn->prepare(
            'SELECT ub.*,ct.name,ct.icon,ct.color,amount_spent FROM user_budgets as ub LEFT JOIN view_budgets_amount_spent vbas on ub.id = vbas.budget_id LEFT JOIN categories_types ct on ub.category_type_id = ct.id WHERE ub.user_id = :userId'
        );

        $stmt->execute([$userId]);
        $budgets = $stmt->fetchAll(PDO::FETCH_ASSOC);


        $budgetsData = [];
        foreach ($budgets as $budget){
            if($budget['amount_spent'] == null){
                $budget['amount_spent'] = 0;
            }
            array_push($budgetsData,
            array(
                'budgetId' => $budget['id'],
                'userId' => $budget['user_id'],
                'categoryTypeId' => $budget['category_type_id'],
                'value' => $budget['value'],
                'amountSpent' => $budget['amount_spent'],
                'startDate' => $budget['start_date'],
                'endDate' => $budget['end_date'],
                'name' => $budget['name'],
                'icon' => $budget['icon'],
                'color' => $budget['color'],
                'permission' => true
            ));
        }
        return $budgetsData;
    }

    function deleteBudget($budgetId){
        $conn = $this->database->connect();
        $stmt = $conn->prepare(
            'DELETE FROM user_budgets WHERE id = :budgetId'
        );
        $stmt->execute([$budgetId]);
    }
}