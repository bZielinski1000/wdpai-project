<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Transaction.php';

class TransactionRepository extends Repository
{
    public function getTransactions(int $categoryId): ?array
    {
        $stmt = $this->database->connect()->prepare(
            "SELECT * from user_transactions where category_id = :categoryId"
        );
        $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
        $stmt->execute();

        $transactions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($transactions == false){
            return null;
        }

        $transactionsData =[];
        foreach ($transactions as $transaction){
            array_push($transactionsData,
                new Transaction(
                    $transaction['id'],
                    $transaction['category_id'],
                    $transaction['name'],
                    $transaction['value'],
                    $transaction['date']
                )
            );
        }

        if($transactionsData != null){
            usort($transactionsData, fn($a, $b) => strcmp($a->getDate(), $b->getDate()));
        }

        return array_map(function ($transaction) {
            return array(
                'transactionId' => $transaction->getId(),
                'categoryId' => $transaction->getCategoryId(),
                'name' => $transaction->getName(),
                'value' => $transaction->getValue(),
                'date' => $transaction->getFormattedDate(),
                'day' => $transaction->getDay()
            );
        },$transactionsData);
    }

    public function insertTransaction(int $categoryId, string $name, float $value, $date){
        $conn = $this->database->connect();
        $stmt = $conn->prepare(
            'INSERT INTO user_transactions(category_id,"name","value","date") VALUES (:categoryId,:name,:value,:date)'
        );
        $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':value', $value, PDO::PARAM_INT);
        $stmt->bindParam(':date', $date, PDO::PARAM_STR);
        $stmt->execute();

        $transactionId = $conn->lastInsertId();

        $transaction = new Transaction(
            $transactionId,
            $categoryId,
            $name,
            $value,
            $date
        );

        return array(
            'transactionId' => $transaction->getId(),
            'categoryId' => $transaction->getCategoryId(),
            'name' => $transaction->getName(),
            'value' => $transaction->getValue(),
            'date' => $transaction->getFormattedDate(),
            'day' => $transaction->getDay()
        );

    }

    public function deleteTransaction(int $id){
        $conn = $this->database->connect();
        $stmt = $conn->prepare(
            'Delete from user_transactions where id = :transition_id;'
        );
        $stmt->bindParam(':transition_id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return array('transactionId' => $id);
    }
}