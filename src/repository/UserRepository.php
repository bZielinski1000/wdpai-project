<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

class UserRepository extends Repository
{
    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT users.*,ur.permission FROM public.users LEFT JOIN users_roles ur on users.role_id = ur.id WHERE users.email = :email'
        );

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false){
            return null;
        }

        return new User(
            $user['id'],
            $user['email'],
            $user['password'],
            $user['name'],
            $user['permission']
        );
    }

    public function insertUser(string $userName,string $email,string $password)
    {
        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public.users(name,email,password) VALUES (?,?,?)'
        );
        $stmt->execute([$userName,$email,$password]);
    }
}