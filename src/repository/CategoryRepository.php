<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Category.php';

class CategoryRepository extends Repository
{
    public function getCategories(string $email, int $month, int $year): ?array
    {
        $stmt = $this->database->connect()->prepare(
            "Select  cat_t.*,u_cat.date,u_cat.id,cv.category_value from categories_types cat_t LEFT JOIN user_categories u_cat on cat_t.id = u_cat.category_type_id LEFT JOIN users u on u.id = u_cat.user_id LEFT JOIN view_categories_value cv on u_cat.id = cv.category_id where u.email = :email AND date_part('month',u_cat.date) = :month AND date_part('year',u_cat.date) = :year"
        );

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':month', $month, PDO::PARAM_INT);
        $stmt->bindParam(':year', $year, PDO::PARAM_INT);
        $stmt->execute();

        $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($categories == false){
            return null;
        }

        return $categories;
    }

    public function insertCategory(int $userId,int $type,string $date)
    {
        $conn = $this->database->connect();
        $stmt = $conn->prepare(
            'INSERT INTO public.user_categories(user_id,category_type_id,date) VALUES (?,?,?)'
        );
        $stmt->execute([$userId,$type,$date]);
        $categoryId = $conn->lastInsertId();

        $category = $this->getCategory($categoryId);
        return array(
            'id' => $category->getId(),
            'name' => $category->getName(),
            'icon' => $category->getIcon(),
            'color' => $category->getColor(),
            'date' => $category->getDate()
        );
    }

    public function getCategory(int $categoryId): Category{
        $stmt = $this->database->connect()->prepare(
            'Select uc.*,ct.*,cv.category_value from user_categories uc LEFT JOIN categories_types ct on ct.id = uc.category_type_id LEFT JOIN view_categories_value cv on uc.id = cv.category_id where uc.id = :categoryId'
        );
        $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
        $stmt->execute();

        $category = $stmt->fetch(PDO::FETCH_ASSOC);

        return new Category(
            $categoryId,
            $category['name'],
            $category['icon'],
            $category['color'],
            $category['date'],
            $category['category_value']
        );
    }

    public function deleteCategory(int $id){
        $conn = $this->database->connect();
        $stmt = $conn->prepare(
            'Delete from user_categories where id = :category_id;'
        );
        $stmt->bindParam(':category_id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $stmt = $conn->prepare(
            'Delete from user_transactions where category_id = :category_id;'
        );
        $stmt->bindParam(':category_id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return array('id' => $id);
    }
}