<?php
require_once 'AppController.php';
require_once __DIR__ . '/../repository/CategoryRepository.php';
require_once __DIR__ . '/../repository/UserRepository.php';
require_once __DIR__ .'/../models/Category.php';
require_once __DIR__ .'/../models/User.php';

class CategoryController extends AppController
{
    private $categoryRepository;
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->categoryRepository = new CategoryRepository();
        $this->userRepository = new UserRepository();
    }

    public function addCategory()
    {
        if (!$this->isPost()) {
            die('Wrong url');
        }

        $decoded = $this->handleRequest();
        if($decoded){
            $user = $this->userRepository->getUser($_COOKIE['userSession']);
            $fullDate = $decoded['date'].'-01';
            $category = $this->categoryRepository->insertCategory($user->getId(),$decoded['option'],$fullDate);

            echo json_encode($category);
        }
    }

    public function getCategories(){
        if (!$this->isPost()) {
            die('Wrong url');
        }

        $decoded = $this->handleRequest();
        if($decoded){
            $email = $_COOKIE['userSession'];
            $cat = $this->categoryRepository->getCategories($email,$decoded["month"],$decoded["year"]);

            echo json_encode($cat);
        }

    }

    public function deleteCategory(){
        if (!$this->isDelete()) {
            die('Wrong url');
        }

        $decoded = $this->handleRequest();
        if($decoded){
            $categoryId = $this->categoryRepository->deleteCategory($decoded["categoryId"]);

            echo json_encode($categoryId);
        }
    }
}