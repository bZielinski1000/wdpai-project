<?php
require_once 'AppController.php';
require_once __DIR__ . '/../repository/TransactionRepository.php';
require_once __DIR__ . '/../repository/CategoryRepository.php';
require_once __DIR__ . '/../repository/UserRepository.php';
require_once __DIR__ . '/../models/Transaction.php';
require_once __DIR__ . '/../models/User.php';

class TransactionsController extends AppController
{
    private $transactionRepository;
    private $categoryRepository;
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->transactionRepository = new TransactionRepository();
        $this->categoryRepository = new CategoryRepository();
        $this->userRepository = new UserRepository();
    }

    public function transaction()
    {
        if (!$this->isGet()) {
            die('Wrong url');
        }

        if (isset($_COOKIE['userSession'])) {
            $email = $_COOKIE['userSession'];
            $user = $this->userRepository->getUser($email);

            $categoryId = $_GET["catId"];
            if($categoryId == null){
                die("Wrong url!");
            }

            $category = $this->categoryRepository->getCategory($categoryId);


            $this->render('single-transaction',
                [
                    'userName' => $user->getName(),
                    'category' => $category
                ]);
        } else {
            $this->render('login');
        }
    }

    public function getTransactions()
    {
        if (!$this->isGet()) {
            die('Wrong url');
        }

        if (isset($_COOKIE['userSession'])) {
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

            if ($contentType === "application/json") {
                header('Content-type: application/json');
                http_response_code(200);


                $categoryId = $_GET["catId"];
                $transactions = $this->transactionRepository->getTransactions($categoryId);
                echo json_encode($transactions);
            }
        }else{
            $this->render('login');
        }
    }

    public function addTransaction()
    {
        if (!$this->isPost()) {
            die('Wrong url');
        }

        $decoded = $this->handleRequest();
        if($decoded){
            $transaction = $this->transactionRepository->insertTransaction(
                $decoded['categoryId'],
                $decoded['name'],
                floatval($decoded['value']),
                $decoded['date']
            );

            echo json_encode($transaction);
        }
    }

    public function deleteTransaction()
    {
        if (!$this->isDelete()) {
            die('Wrong url');
        }
        $decoded = $this->handleRequest();
        if($decoded){
            $transactionId = $this->transactionRepository->deleteTransaction($decoded['transactionId']);
            echo json_encode($transactionId);
        }
    }

}