<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__ .'/../models/User.php';

class LoginController extends AppController
{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function loginView()
    {
        if(!$this->isGet()){
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/category");
        }else{
            $this->render('login');
        }
    }

    public function login()
    {
        if (!$this->isPost()) {
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/category");
        }


        $email = $_POST['email'];
        $password = $_POST['password'];

        $user = $this->userRepository->getUser($email);

        if ($user === null || $user->getEmail() !== $email) {
            return $this->render('login', ['messages' => ['User with this email not exist!']]);
        }

        if (!password_verify($password,$user->getPassword())) {
            return $this->render('login', ['messages' => ['Wrong password!']]);
        }

        //Setting cookie
        setcookie('userSession',$user->getEmail(),time()+3600);

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/category");
    }

    public function logout(){
        if (!$this->isPost()) {
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){
            setcookie('userSession',null,time()-300);
        }

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}");
    }

    public function register()
    {
        if (!$this->isPost()) {
            die('Wrong url');
        }

        $userName = $_POST['username'];
        $email = $_POST['email'];
        $password = password_hash($_POST['password'],PASSWORD_DEFAULT);


        $this->userRepository->insertUser($userName,$email,$password);

        return $this->render('registration',['accepted' => [$userName]]);
    }
}