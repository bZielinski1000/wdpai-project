<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../repository/CategoryRepository.php';
require_once __DIR__ .'/../models/User.php';
require_once __DIR__ .'/../models/Category.php';

class DefaultController extends AppController {
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }


    public function index()
    {
        if (!$this->isGet()) {
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){

            $email = $_COOKIE['userSession'];
            $user = $this->userRepository->getUser($email);

            $this->render('category',
                [
                    'userName' => $user->getName()
                ]);
        }else {
            $this->render('login');
        }
    }

    public function registration()
    {
        if (!$this->isGet()) {
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/category");
        }
        $this->render('registration');
    }

    public function category()
    {
        if (!$this->isGet()) {
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){

            $email = $_COOKIE['userSession'];
            $user = $this->userRepository->getUser($email);

            $this->render('category',
                [
                    'userName' => $user->getName()
                ]);
        }else {
            $this->render('login');
        }
    }


    public function budget()
    {
        if (!$this->isGet()) {
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){
            $email = $_COOKIE['userSession'];
            $user = $this->userRepository->getUser($email);
            if($user->getPermission() < 2){
                $this->render('budget',['userName' => $user->getName(),'isPremium' => true]);
            }else{
                $this->render('budget',['userName' => $user->getName(),'isPremium' => false]);
            }
        }else {
            $this->render('login');
        }
    }

    public function summary()
    {
        if (!$this->isGet()) {
            die('Wrong url');
        }

        if(isset($_COOKIE['userSession'])){
            $email = $_COOKIE['userSession'];
            $user = $this->userRepository->getUser($email);
            $this->render('summary',['userName' => $user->getName()]);
        }else {
            $this->render('login');
        }
    }
}