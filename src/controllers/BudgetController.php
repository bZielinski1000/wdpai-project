<?php
require_once 'AppController.php';
require_once __DIR__ . '/../repository/CategoryRepository.php';
require_once __DIR__ . '/../repository/BudgetRepository.php';
require_once __DIR__ . '/../repository/UserRepository.php';
require_once __DIR__ .'/../models/Category.php';
require_once __DIR__ .'/../models/Budget.php';
require_once __DIR__ .'/../models/User.php';

class BudgetController extends AppController
{
    private $budgetRepository;
    private $userRepository;


    public function __construct()
    {
        parent::__construct();
        $this->budgetRepository = new BudgetRepository();
        $this->userRepository = new UserRepository();
    }


    function addBudget(){
        if(!$this->isPost()){
            die('Wrong url');
        }

        $decoded = $this->handleRequest();
        if($decoded){
            $user = $this->userRepository->getUser($_COOKIE['userSession']);
            if($user->getPermission() < 2){
                $budget = $this->budgetRepository->insertBudget(
                    $user->getId(),
                    $decoded['categoryTypeId'],
                    floatval($decoded['value']),
                    $decoded['startDate'],
                    $decoded['endDate']
                );
                echo json_encode($budget);
            }
            else{
                echo json_encode(array(
                    'noPermission' => true
                ));
            }
        }
    }

    function getBudgets(){
        if(!$this->isGet()){
            die('Wrong url');
        }

        if (isset($_COOKIE['userSession'])) {
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

            if ($contentType === "application/json") {
                $content = trim(file_get_contents("php://input"));
                $decoded = json_decode($content, true);

                header('Content-type: application/json');
                http_response_code(200);

                $user = $this->userRepository->getUser($_COOKIE['userSession']);
                if($user->getPermission() < 2) {
                    $budget = $this->budgetRepository->getBudgets($user->getId());
                    echo json_encode($budget);
                }
                else{
                    echo json_encode(array(
                        'noPermission' => true
                    ));
                }
            }
        }else{
            $this->render('login');
        }
    }

    function deleteBudget(){
        if(!$this->isDelete()){
            die('Wrong url');
        }

        $decoded = $this->handleRequest();
        if($decoded){
            $user = $this->userRepository->getUser($_COOKIE['userSession']);
            if($user->getPermission() < 2) {
                $this->budgetRepository->deleteBudget($decoded['budgetId']);
                echo json_encode($decoded['budgetId']);
            }
            else{
                echo json_encode(array(
                    'permission' => false
                ));
            }
        }
    }
}