<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/single-transaction.css">
    <link rel="stylesheet" type="text/css" href="/public/css/components/menuButton.css">
    <link rel="stylesheet" type="text/css" href="/public/css/components/loader.css">
    <script src="https://kit.fontawesome.com/4c8c850f0a.js" crossorigin="anonymous"></script>
    <script rel="script" src="/public/js/fetchWrapper.js" defer></script>
    <script rel="script" src="/public/css/components/menuButton.js" defer></script>
    <script rel="script" src="/public/js/transactionScript.js" defer></script>
    <title>Transaction</title>
</head>
<body>
<div class="add-window">
    <div class="add-window-nav">
        <h2>Add Transaction</h2>
        <i class="fas fa-times closed-icon"></i>
    </div>
    <div class="add-transaction">
        <div>
            <label for="name">Name</label>
            <input type="text" name="name" required>
        </div>
        <div>
            <label for="value">Value</label>
            <input type="number" name="value" step="0.01" required>
        </div>
        <div class="date-container">
            <label for="date">Date</label>
            <?php
            if (isset($category)) {
                echo '<input type="date" name="date" min="'. $category->getMinDate().'" max="'. $category->getMaxDate().'" required>';
            }
            ?>
        </div>
        <button type="button">CONFIRM</button>
    </div>
</div>
<div class="base-container">
    <nav>
        <ul>
            <li>
                <a href="category">
                    <div class="nav-item-container">
                        <i class="fas fa-wallet"></i>
                        <p>Transactions</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="budget">
                    <div class="nav-item-container">
                        <i class="fas fa-piggy-bank"></i>
                        <p>Budget</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="summary">
                    <div class="nav-item-container">
                        <i class="fas fa-file-alt"></i>
                        <p>Summary</p>
                    </div>
                </a>
            </li>
            <li>
                <div class="nav-item-container">
                    <form action="logout" method="post">
                        <button type="submit">SIGN OUT</button>
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <main>
        <header id="main-header">
            <div>
                <button class="menu-btn"></button>
                <h2>Transactions</h2>
            </div>
            <div>
                <?php
                if (isset($userName)) {
                    echo '<div>
                                <i class="fas fa-user-alt"></i>
                                <h3>' . $userName . '</h3>
                          </div>';
                }
                ?>
                <button class="add-window-btn">ADD TRANSACTION</button>
            </div>
        </header>
        <section class="transactions">
            <header>
                <?php
                if (isset($category)) {
                    echo '
                    <div class="inner-header">
                        <div class="big-circle" style="background: ' . $category->getColor() . '"><i class="fas ' . $category->getIcon() . '"></i></div>
                        <h2>' . $category->getName() . '</h2>
                    </div>
                    <div class="inner-header">
                        <p class="category-value ' . $category->isValuePositive() .'">' . $category->getValue() .'zł</p>
                        <a href="category"><i class="fas fa-angle-left"></i></a>
                    </div>
                        ';
                }
                ?>
            </header>
            <div class="transactions-container">
                <div class="loader">
                    <div></div>
                    <div></div>
                </div>
                <div class="no-data-icon">
                    <i class="fas fa-sad-tear no-data-icon"></i>
                    <span>Ups... No data found</span>
                </div>
            </div>
        </section>
    </main>
</div>

<template id="transaction-template">
    <div class="transaction" id="">
        <div class="tran">
            <div class="tran-header">
                <h1></h1>
                <div>
                    <span class="tra-name"></span>
                    <span class="tra-date"></span>
                </div>
            </div>
            <div class="tran-footer">
                <p></p>
                <div class="red-square">
                    <i class="fas fa-trash"></i>
                </div>
            </div>
        </div>
    </div>
</template>
</body>
</html>