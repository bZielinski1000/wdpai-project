<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="public/css/login.css">
    <script src="https://kit.fontawesome.com/4c8c850f0a.js" crossorigin="anonymous"></script>
    <title>Login</title>
</head>
<body>
<div id="login-container">
    <div class="title">
        <i class="fas fa-wallet"></i>
        My Little Savings
    </div>
    <form class="login-form" action="login" method="post">
        <p class="welcome">Welcome back</p>
        <h2>Log into your account</h2>

        <div class="messages">
            <?php
            if(isset($messages)){
                foreach($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </div>

        <div class="label-container">
            <label for="email"><span>E-mail</span></label>
            <input name="email" type="text" placeholder="Enter your email" id="name">
        </div>

        <div class="label-container">
            <label for="password"><span>Password</span> <a href="#" id="forgot-pass">Forgot password?</a></label>
            <div class="input-container">
                <a href="#"><i class="fas fa-eye icon-for-input"></i></a>
                <input name="password" type="password" placeholder="Enter your password" id="password">
            </div>
        </div>

        <div class="div-button">
            <button type="submit">Login now</button>
            <p>Not registered yet? <a href="registration">Register!</a></p>
        </div>
    </form>
</div>
</body>
</html>