<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/budget.css">
    <link rel="stylesheet" type="text/css" href="/public/css/components/menuButton.css">
    <link rel="stylesheet" href="/public/css/components/custom-Select.css">
    <link rel="stylesheet" href="/public/css/components/loader.css">
    <script src="https://kit.fontawesome.com/4c8c850f0a.js" crossorigin="anonymous"></script>
    <script rel="script" src="/public/css/components/custom-Select.js" defer></script>
    <script rel="script" src="/public/css/components/menuButton.js" defer></script>
    <?php
    if(isset($isPremium) && $isPremium){
        echo '<script rel="script" src="/public/js/fetchWrapper.js" defer></script>';
        echo '<script rel="script" src="/public/js/budgetScript.js" defer></script>';
    }
    ?>
    <title>Budgets</title>
</head>
<body>
<div class="add-window">
    <div class="add-window-nav">
        <h2>Add budget</h2>
        <i class="fas fa-times closed-icon"></i>
    </div>
    <div class="wrapper">
        <p>Budget</p>
        <div class="select_wrap">
            <ul class="default_option">
                <li>
                    <div class="option option1" id="1">
                        <div class="circle"><i class="fas fa-shopping-cart"></i></div>
                        <p>Shopping</p>
                    </div>
                </li>
            </ul>
            <ul class="select_ul">
                <li>
                    <div class="option option1" id="1">
                        <div class="circle"><i class="fas fa-shopping-cart"></i></div>
                        <p>Shopping</p>
                    </div>
                </li>
                <li>
                    <div class="option option2" id="2">
                        <div class="circle"><i class="fas fa-file-invoice-dollar"></i></div>
                        <p>Bills</p>
                    </div>
                </li>
                <li>
                    <div class="option option3" id="3">
                        <div class="circle"><i class="fas fa-money-bill-wave"></i></div>
                        <p>Salary</p>
                    </div>
                </li>
                <li>
                    <div class="option option4" id="4">
                        <div class="circle"><i class="fas fa-plane"></i></div>
                        <p>Travel</p>
                    </div>
                </li>
                <li>
                    <div class="option option5" id="5">
                        <div class="circle"><i class="fas fa-smile-beam"></i></div>
                        <p>Entertainment</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="input-container">
        <label for="value">Value</label>
        <input type="number" name="value" step="0.01" min="0.01" required>
    </div>
    <div class="date-container">
        <label for="date">From</label>
        <input type="date" name="date">
    </div>
    <div class="date-container">
        <label for="date">To</label>
        <input type="date" name="date">
    </div>
    <button>CONFIRM</button>
</div>
<div class="base-container">
    <nav>
        <ul>
            <li>
                <a href="category">
                    <div class="nav-item-container">
                        <i class="fas fa-wallet"></i>
                        <p>Transactions</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="budget">
                    <div class="nav-item-container">
                        <i class="fas fa-piggy-bank"></i>
                        <p>Budget</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="summary">
                    <div class="nav-item-container">
                        <i class="fas fa-file-alt"></i>
                        <p>Summary</p>
                    </div>
                </a>
            </li>
            <li>
                <div class="nav-item-container">
                    <form action="logout" method="post">
                        <button type="submit">SIGN OUT</button>
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <main>
        <header id="main-header">
            <div>
                <button class="menu-btn"></button>
                <h2>Budgets</h2>
            </div>
            <div class="main-header-second-div">
                <?php
                    if (isset($userName)) {
                        echo '<div>
                                <i class="fas fa-user-alt"></i>
                                <h3>' . $userName . '</h3>
                          </div>';
                    }
                    if(isset($isPremium) && $isPremium){
                        echo '<button class="add-window-btn">ADD BUDGET</button>';
                    }
                ?>
            </div>
        </header>
        <section class="budgets">
            <header>
                <div><span>YOUR BUDGETS</span></div>
            </header>
            <div class="budgets-container">
                <?php
                    if(isset($isPremium) && $isPremium){
                        echo '  <div class="loader">
                                    <div></div>
                                    <div></div>
                                </div>
                                <div class="no-data-icon">
                                    <i class="fas fa-sad-tear no-data-icon"></i>
                                    <span>Ups... No data found</span>
                                </div>';
                    }else{
                        echo'<h1 class="no-permission">This feature is designated for premium users</h1>';
                    }
                ?>
            </div>
        </section>
    </main>
</div>

<template id="budget-template">
    <div class="budg" id="">
        <div class="circle"><i class="fas "></i></div>
        <div class="budg-inner-cont">
            <div>
                <h2></h2>
                <div class="budg-dates">
                    <p><span>From:</span> 2021-01-01</p>
                    <p><span>To:</span> 2021-01-02</p>
                </div>
            </div>
            <div class="budg-left-inner-cont">
                <div class="budg-walet">
                    <span class="plus"></span>
                    <span></span>
                </div>
                <div class="red-square">
                    <i class="fas fa-trash"></i>
                </div>
            </div>
            <div class="progress-bar" style="--progress: 20%;">
                <div class="pointer"></div>
            </div>
        </div>
    </div>
</template>
</body>
</html>