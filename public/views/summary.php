<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/summary.css">
    <link rel="stylesheet" type="text/css" href="/public/css/components/menuButton.css">
    <script src="https://kit.fontawesome.com/4c8c850f0a.js" crossorigin="anonymous"></script>
    <script rel="script" src="/public/css/components/menuButton.js" defer></script>
    <title>Summary</title>
</head>
<body>
<div class="base-container">
    <nav>
        <ul>
            <li>
                <a href="category">
                    <div class="nav-item-container">
                        <i class="fas fa-wallet"></i>
                        <p>Transactions</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="budget">
                    <div class="nav-item-container">
                        <i class="fas fa-piggy-bank"></i>
                        <p>Budget</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="summary">
                    <div class="nav-item-container">
                        <i class="fas fa-file-alt"></i>
                        <p>Summary</p>
                    </div>
                </a>
            </li>
            <li>
                <div class="nav-item-container">
                    <form action="logout" method="post">
                        <button type="submit">SIGN OUT</button>
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <main>
        <header id="main-header">
            <div>
                <button class="menu-btn"></button>
                <h2>Summary</h2>
            </div>
            <div class="date">
                <?php
                if (isset($userName)) {
                    echo '<div>
                                <i class="fas fa-user-alt"></i>
                                <h3>' . $userName . '</h3>
                          </div>';
                }
                ?>
                <span>This month</span>
                <i class="fas fa-caret-down"></i>
            </div>
        </header>
        <section class="summary">
            <header>
                <div class="balance">
                    <h2>Inflow</h2>
                    <div>
                        <span class="plus">+3000.00zł</span>
                        <i class="fas fa-long-arrow-alt-up plus"></i>
                    </div>
                </div>
                <div class="balance">
                    <h2>Outflow</h2>
                    <div>
                        <span class="minus">-2569.00zł</span>
                        <i class="fas fa-long-arrow-alt-down minus"></i>
                    </div>
                </div>
                <div class="balance">
                    <h2>Balance</h2>
                    <div>
                        <span class="plus">+431.00zł</span>
                        <i class="fas fa-long-arrow-alt-up plus"></i>
                    </div>
                </div>
            </header>
            <div class="summary-details">
                <div class="earnings">
                    <h2>Earnings</h2>
                    <div class="diagram">
                        <div class="piece"></div>
                        <div class="piece"></div>
                    </div>
                    <div class="components">
                        <div class="comp" id="comp-1">
                            <div><i class="fas fa-money-bill-wave"></i></div>
                            <span>Salary</span>
                        </div>
                        <div class="comp" id="comp-2">
                            <div><i class="fas fa-gift"></i></div>
                            <span>Gifts</span>
                        </div>
                    </div>
                </div>
                <div class="spendings">
                    <h2>Spendings</h2>
                    <div class="diagram">
                        <div class="piece"></div>
                        <div class="piece"></div>
                        <div class="piece"></div>
                    </div>
                    <div class="components">
                        <div class="comp" id="comp-3">
                            <div><i class="fas fa-file-invoice-dollar"></i></div>
                            <span>Bills</span>
                        </div>
                        <div class="comp" id="comp-4">
                            <div><i class="fas fa-shopping-cart"></i></div>
                            <span>Shopping</span>
                        </div>
                        <div class="comp" id="comp-5">
                            <div><i class="fas fa-smile-beam"></i></div>
                            <span>Entertainment</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
</body>
</html>