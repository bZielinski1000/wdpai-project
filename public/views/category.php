<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/public/css/style.css">
    <link rel="stylesheet" href="/public/css/transactions.css">
    <link rel="stylesheet" href="/public/css/components/menuButton.css">
    <link rel="stylesheet" href="/public/css/components/custom-Select.css">
    <link rel="stylesheet" href="/public/css/components/loader.css">
    <script src="https://kit.fontawesome.com/4c8c850f0a.js" crossorigin="anonymous"></script>
    <script rel="script" src="/public/js/fetchWrapper.js" defer></script>
    <script rel="script" src="/public/css/components/menuButton.js" defer></script>
    <script rel="script" src="/public/js/categoryScript.js" defer></script>
    <script rel="script" src="/public/css/components/custom-Select.js" defer></script>
    <title>Transactions</title>
</head>
<body>
<div class="add-window">
    <div class="add-window-nav">
        <h2>Add category</h2>
        <i class="fas fa-times closed-icon"></i>
    </div>

    <div class="wrapper">
        <p>Category</p>
        <div class="select_wrap">
            <ul class="default_option">
                <li>
                    <div class="option option1" id="1">
                        <div class="circle"><i class="fas fa-shopping-cart"></i></div>
                        <p>Shopping</p>
                    </div>
                </li>
            </ul>
            <ul class="select_ul">
                <li>
                    <div class="option option1" id="1">
                        <div class="circle"><i class="fas fa-shopping-cart"></i></div>
                        <p>Shopping</p>
                    </div>
                </li>
                <li>
                    <div class="option option2" id="2">
                        <div class="circle"><i class="fas fa-file-invoice-dollar"></i></div>
                        <p>Bills</p>
                    </div>
                </li>
                <li>
                    <div class="option option3" id="3">
                        <div class="circle"><i class="fas fa-money-bill-wave"></i></div>
                        <p>Salary</p>
                    </div>
                </li>
                <li>
                    <div class="option option4" id="4">
                        <div class="circle"><i class="fas fa-plane"></i></div>
                        <p>Travel</p>
                    </div>
                </li>
                <li>
                    <div class="option option5" id="5">
                        <div class="circle"><i class="fas fa-smile-beam"></i></div>
                        <p>Entertainment</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="date-container">
        <label for="date">Date</label>
        <input type="month" name="date">
    </div>
    <button>CONFIRM</button>
</div>
<div class="base-container">
    <nav>
        <ul>
            <li>
                <a href="category">
                    <div class="nav-item-container">
                        <i class="fas fa-wallet"></i>
                        <p>Transactions</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="budget">
                    <div class="nav-item-container">
                        <i class="fas fa-piggy-bank"></i>
                        <p>Budget</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="summary">
                    <div class="nav-item-container">
                        <i class="fas fa-file-alt"></i>
                        <p>Summary</p>
                    </div>
                </a>
            </li>
            <li>
                <div class="nav-item-container">
                    <form action="logout" method="post">
                        <button type="submit">SIGN OUT</button>
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <main>
        <header id="main-header">
            <div>
                <button class="menu-btn"></button>
                <h2>Transactions</h2>
            </div>
            <div class="main-header-second-div">
                <input type="month" name="search">
                <?php
                if (isset($userName)) {
                    echo '<div>
                                <i class="fas fa-user-alt"></i>
                                <h3>' . $userName . '</h3>
                          </div>';
                }
                ?>
                <button class="add-window-btn">ADD CATEGORY</button>
            </div>
        </header>
        <section class="categories">
            <header>
                <div><span>Last month</span></div>
                <div class="selected"><span>This month</span></div>
                <div><span>Next month</span></div>
            </header>
            <div class="categories-container">
                <div class="loader">
                    <div></div>
                    <div></div>
                </div>
                <div class="no-data-icon">
                    <i class="fas fa-sad-tear no-data-icon"></i>
                    <span>Ups... No data found</span>
                </div>
            </div>
        </section>
    </main>
</div>

<template id="category-template">
    <a href="#" class="category-link" id="">
        <div class="cat">
            <div class="cat-inner-div">
                <div class="circle"><i class="fas "></i></div>
                <h2>Name</h2>
            </div>
            <div class="cat-inner-div">
                <span class="cat-value">value</span>
                <div class="red-square">
                    <i class="fas fa-trash"></i>
                </div>
                <i class="fas fa-angle-right"></i>
            </div>
        </div>
    </a>
</template>
</body>
</html>