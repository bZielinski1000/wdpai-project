<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="public/css/login.css">
    <link rel="stylesheet" type="text/css" href="public/css/registration.css">
    <script src="https://kit.fontawesome.com/4c8c850f0a.js" crossorigin="anonymous"></script>
    <script rel="script" src="/public/js/popupButton.js" defer></script>
    <script rel="script" src="/public/js/validateRegistration.js" defer></script>
    <title>Registration</title>
</head>
<body>

<?php
if (isset($accepted)) {
    echo '<div class="popup"> 
         <span>Created account ' . $accepted[0] . '</span>
         <button class="popup-btn">Close</button>
        </div>';
}
?>

<div id="login-container">
    <div class="title">
        <i class="fas fa-wallet"></i>
        My Little Savings
    </div>
    <form class="login-form" action="register" method="post">
        <p class="welcome">Nice to see you</p>
        <h2>Create an account</h2>
        <div class="label-container">
            <label for="username"><span>Username</span></label>
            <input name="username" type="text" placeholder="Username" id="username" required>
        </div>

        <div class="label-container">
            <label for="email"><span>E-mail</span></label>
            <input name="email" type="text" placeholder="Email" id="email" required>
        </div>

        <div class="label-container">
            <label for="password"><span>Password</span></label>
            <div class="input-container">
                <a href="#"><i class="fas fa-eye icon-for-input"></i></a>
                <input name="password" type="password" placeholder="Password" id="password" required>
            </div>
        </div>

        <div class="label-container">
            <label for="r-password"><span>Repeat password</span></label>
            <div class="input-container">
                <a href="#"><i class="fas fa-eye icon-for-input"></i></a>
                <input name="r-password" type="password" placeholder="Repeat password" id="r-password" required>
            </div>
        </div>

        <div class="div-button">
            <button type="submit">Register</button>
            <p>Already have an account? <a href="loginView">Login here!</a></p>
        </div>
    </form>
</div>
</body>
</html>