const defaultOption = document.querySelector(".default_option");
const selectWrap = document.querySelector(".select_wrap");


defaultOption.addEventListener("click",()=>{
    selectWrap.classList.toggle('active');
})

const options = document.querySelectorAll('.select_ul li');
const defaultOptionLi = document.querySelector(".default_option li");

for(let option of options){
    option.addEventListener("click",()=>{
        defaultOptionLi.innerHTML = option.innerHTML;
        selectWrap.classList.remove('active');
    })
}

