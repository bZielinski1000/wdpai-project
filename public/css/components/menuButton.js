const menuBtn = document.querySelector('.menu-btn');
const navigation = document.querySelector('.base-container>nav');
menuBtn.addEventListener('click',() =>{
    menuBtn.classList.toggle('open');
    navigation.classList.toggle('nav-open');
})