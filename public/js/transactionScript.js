const submitButton = document.querySelector('.add-window button');
const inputs = document.querySelectorAll('.add-transaction input');
const noDataIcon = document.querySelector('.no-data-icon');

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const categoryId = urlParams.get('catId');

const myHTMLNumberInput = inputs[1];
myHTMLNumberInput.onchange = setTwoNumberDecimal;
function setTwoNumberDecimal(){
    this.value = parseFloat(this.value).toFixed(2);
}

const loader = document.querySelector('.loader');

fetchWrapper(
    `/getTransactions?catId=${categoryId}`,
    null,
    'GET',
    (transactions)=>{
        if(transactions !== null) {
            transactions.forEach((transaction) => {
                createTransaction(transaction);
            })
        }else{
            noDataIcon.style.display = 'block';
        }
        loader.style.display = 'none';
    }
)

submitButton.addEventListener('click', function () {
    if(validateForm()) {
        const submitButton = this;
        submitButton.style.pointerEvents = "none";


        const dataTransaction = {
            categoryId: categoryId,
            name: inputs[0].value,
            value: inputs[1].value,
            date: inputs[2].value
        }

        fetchWrapper(
            '/addTransaction',
            dataTransaction,
            'POST',
            (transaction)=>{
                createTransaction(transaction);
                updateCategoryValue(transaction.value);
                clearForm();
                submitButton.style.pointerEvents = "auto";
            }
        )
    }
})

function createTransaction(transaction){
    const transactionsContainer = document.querySelector('.transactions-container');
    const template = document.querySelector("#transaction-template");
    const clone = template.content.cloneNode(true);

    const container = clone.querySelector('.transaction');
    const h1 = clone.querySelector('h1');
    const name = clone.querySelector('.tra-name');
    const date = clone.querySelector('.tra-date');
    const value = clone.querySelector('p');
    const transactionButton = clone.querySelector('.red-square');

    container.id = "tran-" + transaction.transactionId;
    h1.innerHTML = transaction.day;
    name.innerHTML = transaction.name;
    date.innerHTML = transaction.date;
    value.innerHTML = transaction.value + 'zł';

    if(transaction.value >= 0){
        value.classList.add('plus');
    }else{
        value.classList.add('minus');
    }

    addTransactionDeleteButtonListener(transactionButton,transaction);


    let index = getPlaceToInsertTransaction(transactionsContainer,transaction.day);
    if(index !== -1){
        transactionsContainer.insertBefore(clone,transactionsContainer.children[index + 2]);
    }else {
        transactionsContainer.appendChild(clone);
    }

    noDataIcon.style.display = 'none';
}

function getPlaceToInsertTransaction(transactionsContainer,newTransitionDate){
    const dates = transactionsContainer.querySelectorAll('.transaction h1');

    for(let i=0;i<dates.length;i++){
        if(parseInt(dates[i].innerText) > parseInt(newTransitionDate)){
            return i;
        }
    }
    return -1;
}

function updateCategoryValue(transactionValue){
    const categoryValue = document.querySelector('.category-value');

    let valueCat = categoryValue.innerHTML.slice(0,-2);
    let updatedValue = parseFloat(transactionValue) + parseFloat(valueCat);
    updatedValue = updatedValue.toFixed(2);

    if(updatedValue >= 0){
        categoryValue.classList.remove('minus');
        categoryValue.classList.add('plus');
        updatedValue = "+" + updatedValue;
    }else {
        categoryValue.classList.remove('plus');
        categoryValue.classList.add('minus');
    }

    categoryValue.innerHTML = updatedValue + 'zł';
}

function addTransactionDeleteButtonListener(transactionButton,transaction){
    transactionButton.addEventListener("click",() =>{
        if(window.confirm("Are you sure that you want to delete this transaction?")){

            fetchWrapper(
                '/deleteTransaction',
                {transactionId: transaction.transactionId},
                "DELETE",
                (transactionId)=>{
                    deleteTransaction(transactionId.transactionId);
                    updateCategoryValue(-transaction.value);
                }
            )
        }
    });
}

function deleteTransaction(transactionId){
    const transactionsContainer = document.querySelector('.transactions-container');
    const transaction = transactionsContainer.querySelector(`#tran-${transactionId}`);

    transactionsContainer.removeChild(transaction);
}

function validateForm() {
    const a = inputs[0].value;
    const b = inputs[1].value;
    const c = inputs[2].value;
    if (a === null || a === "" || b === null || b === "" || c === null || c === "") {
        alert("Please Fill All Required Field");
        return false;
    }
    return true;
}














const addWindowButton = document.querySelector('.add-window-btn');
const addWindow = document.querySelector('.add-window');
const nav = document.querySelector('nav');
const section = document.querySelector('section');
const header = document.querySelector('#main-header');

addWindowButton.addEventListener('click',() =>{
    addWindow.classList.add('add-window-open');
    nav.classList.add('blur');
    section.classList.add('blur');
    header.classList.add('blur');
})

const closedIcon = document.querySelector('.closed-icon');
const formInputs = document.querySelectorAll('.add-window input');
closedIcon.addEventListener('click',()=>{
    addWindow.classList.remove('add-window-open');
    nav.classList.remove('blur');
    section.classList.remove('blur');
    header.classList.remove('blur');

    formInputs.forEach((input) =>{
        input.value = '';
    })
})

function clearForm(){
    addWindow.classList.remove('add-window-open');
    nav.classList.remove('blur');
    section.classList.remove('blur');
    header.classList.remove('blur');

    formInputs.forEach((input) =>{
        input.value = '';
    })
}