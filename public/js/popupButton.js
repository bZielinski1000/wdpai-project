const popupBtn = document.querySelector('.popup-btn');
const popup = document.querySelector('.popup');
if(popupBtn){
    popupBtn.addEventListener('click', () => {
        popup.classList.add('closed');
    })
}
