const dateInput = document.querySelector('.date-container input');
const submitButton = document.querySelector('.add-window button');
const categoryContainer = document.querySelector('.categories-container');
const noDataIcon = document.querySelector('.no-data-icon');
const loader = document.querySelector('.loader');
const headerDivs = document.querySelectorAll('.categories>header>div');
const headerDate = document.querySelector('main>header input[type=month]');


let selectedDate = getDate();
headerDate.value = dateToString(selectedDate);

fetchCategories(selectedDate);

function fetchCategories(body){
    fetchWrapper(
        '/getCategories',
        body,
        'POST',
        (categories)=>{
            loader.style.display = 'none';
            headerDivs.forEach((div) => div.style.pointerEvents = 'auto' );

            if(categories !== null){
                categories.forEach((cat) =>{
                    if(cat.category_value === null){
                        cat.category_value = "0.00";
                    }
                    createCategory(cat,true);
                })
            }

            updateNoDataIcon();
        }
    )
}

headerDate.addEventListener('keyup',function (e){
    if(e.key === 'Enter'){
        let dateArray = headerDate.value.split("-");
        if(dateArray[1] < 10){
            dateArray[1] = dateArray[1].slice(1,2);
        }
        let body = {month: parseInt(dateArray[1]),year: parseInt(dateArray[0])}

        loader.style.display = 'block';
        noDataIcon.style.display = 'none';
        selectedDate = body;
        headerDate.value = dateToString(selectedDate);
        changeHeaders();
        removeCategories();
        fetchCategories(body);
    }
})


submitButton.addEventListener('click', function () {
    if (validateForm()) {
        const submitButton = this;
        submitButton.style.pointerEvents = "none";

        const selectedOption = document.querySelector('.default_option div');

        const data = {
            option: selectedOption.getAttribute('id'),
            date: dateInput.value
        }

        fetchWrapper(
            '/addCategory',
            data,
            "POST",
            (category) =>{
                if (isCategoryVisible(category)) {
                    createCategory(category);
                }
                updateNoDataIcon();
                clearForm();
                submitButton.style.pointerEvents = "auto";
            }
        )
    }
})

function addCategoryLinkListener(link, category) {
    link.addEventListener('click', function (event) {
        if (event.target.classList.contains("fa-trash") || event.target.classList.contains("red-square")) {
            event.preventDefault();

            if (window.confirm("Are you sure that you want to delete this category?")) {

                fetchWrapper(
                    '/deleteCategory',
                    {categoryId: category.id},
                    "DELETE",
                    (categoryId)=>{
                        deleteCategory(categoryId.id);
                        updateNoDataIcon();
                    }
                )
            }
        }

    })
}

function createCategory(category) {
    const template = document.querySelector("#category-template");
    const clone = template.content.cloneNode(true);

    const catContainer = clone.querySelector(".category-link");
    const link = clone.querySelector('a');
    const h2 = clone.querySelector('h2');
    const circle = clone.querySelector('.circle');
    const icon = circle.querySelector('i');
    const span = clone.querySelector('.cat-value');

    catContainer.id = 'cat-' + category.id;

    link.setAttribute('href', "transaction?catId=" + category.id);

    h2.innerHTML = category.name;
    circle.style.background = category.color;
    icon.classList.add(category.icon);

    if (category.category_value >= 0) {
        span.innerHTML = "+" + category.category_value + 'zł';
        span.classList.add('plus');
    } else if (category.category_value === undefined) {
        span.innerHTML = "+0.00zł";
        span.classList.add('plus');
    } else {
        span.innerHTML = category.category_value + 'zł';
        span.classList.add('minus');
    }

    addCategoryLinkListener(link, category);

    categoryContainer.appendChild(clone);
}

function removeCategories() {
    const categories = document.querySelectorAll('.categories-container .category-link');
    categories.forEach((category) => {
        categoryContainer.removeChild(category);
    })
}

function deleteCategory(categoryId) {
    const categoriesContainer = document.querySelector('.categories-container');
    const category = categoriesContainer.querySelector(`#cat-${categoryId}`);

    categoriesContainer.removeChild(category);
}

const categoriesHeaders = document.querySelectorAll(".categories header div");

categoriesHeaders[0].addEventListener("click", () => {
    loader.style.display = 'block';
    noDataIcon.style.display = 'none';
    selectedDate = updateDate(-1);
    headerDate.value = dateToString(selectedDate);
    changeHeaders();
    removeCategories();
    fetchCategories(selectedDate);
})
categoriesHeaders[2].addEventListener("click", () => {
    loader.style.display = 'block';
    noDataIcon.style.display = 'none';
    selectedDate = updateDate(1);
    headerDate.value = dateToString(selectedDate);
    changeHeaders();
    removeCategories();
    fetchCategories(selectedDate);
})

function changeHeaders() {
    let dateToCompare;

    const cond1 = getDate().month === selectedDate.month && getDate().year === selectedDate.year;

    dateToCompare = updateDate(-1);
    const cond2 = getDate().month === dateToCompare.month && getDate().year === dateToCompare.year;

    dateToCompare = updateDate(-1,2);
    const cond3 = getDate().month === dateToCompare.month && getDate().year === dateToCompare.year;

    dateToCompare = updateDate(1);
    const cond4 = getDate().month === dateToCompare.month && getDate().year === dateToCompare.year;

    dateToCompare = updateDate(1,2);
    const cond5 = getDate().month === dateToCompare.month && getDate().year === dateToCompare.year;



    if (cond1){
        categoriesHeaders[0].firstElementChild.innerHTML = 'LAST MONTH';
        categoriesHeaders[1].firstElementChild.innerHTML = 'THIS MONTH';
        categoriesHeaders[2].firstElementChild.innerHTML = 'NEXT MONTH';
    }
    else if(cond2){
        categoriesHeaders[0].firstElementChild.innerHTML = 'THIS MONTH';
        categoriesHeaders[1].firstElementChild.innerHTML = 'NEXT MONTH';
        categoriesHeaders[2].firstElementChild.innerHTML = dateToString(updateDate(1));
    }
    else if(cond3){
        categoriesHeaders[0].firstElementChild.innerHTML = 'NEXT MONTH';
        categoriesHeaders[1].firstElementChild.innerHTML = dateToString(selectedDate);
        categoriesHeaders[2].firstElementChild.innerHTML = dateToString(updateDate(1));
    }
    else if(cond4){
        categoriesHeaders[0].firstElementChild.innerHTML = dateToString(updateDate(-1));
        categoriesHeaders[1].firstElementChild.innerHTML = 'LAST MONTH';
        categoriesHeaders[2].firstElementChild.innerHTML = 'THIS MONTH';
    }
    else if(cond5){
        categoriesHeaders[0].firstElementChild.innerHTML = dateToString(updateDate(-1));
        categoriesHeaders[1].firstElementChild.innerHTML = dateToString(selectedDate);
        categoriesHeaders[2].firstElementChild.innerHTML = 'LAST MONTH';
    }
    else {
        categoriesHeaders[0].firstElementChild.innerHTML = dateToString(updateDate(-1));
        categoriesHeaders[1].firstElementChild.innerHTML = dateToString(selectedDate);
        categoriesHeaders[2].firstElementChild.innerHTML = dateToString(updateDate(1));
    }
}

function updateNoDataIcon() {
    const categories = document.querySelectorAll(".categories-container .category-link");
    let isThereAnyCategory = true;
    categories.forEach((category) => {
        if (!category.classList.contains('cat-hidden')) {
            isThereAnyCategory = false;
        }
    });

    if (isThereAnyCategory) {
        noDataIcon.style.display = 'block';
    } else {
        noDataIcon.style.display = 'none';
    }
}

function updateDate(direction,cycle = 1) {
    let newDate = {
        month: selectedDate.month,
        year: selectedDate.year
    }

    for(let i=0;i<cycle;i++) {
        if (direction === 1) {
            if (newDate.month !== 12) {
                newDate.month += 1;
            } else {
                newDate.month = 1;
                newDate.year += 1;
            }

        } else if (direction === -1) {
            if (newDate.month !== 1) {
                newDate.month -= 1;
            } else {
                newDate.month = 12;
                newDate.year -= 1;
            }
        }
    }
    return newDate;
}

function isCategoryVisible(category) {
    let dateArray = category.date.split("-");
    if (dateArray[1] < 10) {
        dateArray[1] = dateArray[1].slice(1, 2);
    }
    return parseInt(dateArray[0]) === selectedDate.year && parseInt(dateArray[1]) === selectedDate.month;
}

function getDate() {
    let today = new Date();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();

    return {
        month: mm,
        year: yyyy
    }
}

function dateToString(date) {
    if (date.month < 10) {
        return date.year + "-" + "0" + date.month;
    }

    return date.year + "-" + date.month;
}

function validateForm() {
    if (dateInput.value === null || dateInput.value === "") {
        alert("Please Fill All Required Field");
        return false;
    }
    return true;
}


const addWindowButton = document.querySelector('.add-window-btn');
const addWindow = document.querySelector('.add-window');
const nav = document.querySelector('nav');
const section = document.querySelector('section');
const header = document.querySelector('#main-header');

addWindowButton.addEventListener('click', () => {
    addWindow.classList.add('add-window-open');
    nav.classList.add('blur');
    section.classList.add('blur');
    header.classList.add('blur');
})

const closedIcon = document.querySelector('.closed-icon');
const selectContainer = document.querySelector(".select_wrap");
closedIcon.addEventListener('click', () => {
    addWindow.classList.remove('add-window-open');
    nav.classList.remove('blur');
    section.classList.remove('blur');
    header.classList.remove('blur');
    selectContainer.classList.remove('active');
    dateInput.value = '';
})

function clearForm() {
    addWindow.classList.remove('add-window-open');
    nav.classList.remove('blur');
    section.classList.remove('blur');
    header.classList.remove('blur');
    selectContainer.classList.remove('active');
    dateInput.value = '';
}