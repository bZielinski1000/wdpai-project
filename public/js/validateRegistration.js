const form = document.querySelector("form");
const emailInput = form.querySelector('input[name="email"]');
const passwordInput = form.querySelector('input[name="password"]');
const confirmedPasswordInput = form.querySelector('input[name="r-password"]');


function isEmail(email) {
    return /\S+@\S+\.\S+/.test(email);
}

function arePasswordsSame(password, confirmedPassword) {
    return password === confirmedPassword;
}

function markValidation(element, condition) {
    !condition ? element.classList.add('no-valid') : element.classList.remove('no-valid')
}

emailInput.addEventListener('keyup', function () {
    setTimeout(function () {
        markValidation(emailInput, isEmail(emailInput.value));
    }, 1000);
});

confirmedPasswordInput.addEventListener('keyup', function () {
    setTimeout(function () {
        const condition = arePasswordsSame(
            passwordInput.value,
            confirmedPasswordInput.value
        );
        console.log(condition);
        markValidation(confirmedPasswordInput, condition);
    }, 1000);
});

form.addEventListener("submit", e => {
    if(!isEmail(emailInput.value)){
        e.preventDefault();
        alert('Wrong Email');
    }
    else if(!arePasswordsSame(passwordInput.value,confirmedPasswordInput.value)){
        e.preventDefault();
        alert("Password and Repeat Password are different!")
    }
});
