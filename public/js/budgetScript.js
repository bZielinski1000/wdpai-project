const budgetsContainer = document.querySelector('.budgets-container');
const noDataIcon = document.querySelector('.no-data-icon');
const loader = document.querySelector('.loader');

fetchWrapper(
    '/getBudgets',
    null,
    "GET",
    (budgets) => {
        if (budgets) {
            budgets.forEach((budget) => {
                console.log(budget);
                createBudget(budget);
            })
        } else {
            noDataIcon.style.display = 'block';
        }

        loader.style.display = 'none';
    }
)


const submitButton = document.querySelector('.add-window button');
const inputs = document.querySelectorAll('.add-window input');

inputs[0].onchange = setTwoNumberDecimal;

submitButton.addEventListener('click', function () {
    if (validateForm()) {
        submitButton.style.pointerEvents = "none";
        const selectedOption = document.querySelector('.default_option div');

        const data = {
            categoryTypeId: selectedOption.getAttribute('id'),
            value: inputs[0].value,
            startDate: inputs[1].value,
            endDate: inputs[2].value
        }

        fetchWrapper(
            '/addBudget',
            data,
            "POST",
            (budget) => {
                console.log(budget);
                createBudget(budget);
                closeAddWindow();
                clearForm();
                submitButton.style.pointerEvents = "auto";
            }
        )
    }
})

function addDeleteListener(deleteButton,budgetId){
    deleteButton.addEventListener('click',()=>{
        fetchWrapper(
            '/deleteBudget',
            {budgetId: budgetId},
            "DELETE",
            () =>{
                const budget = document.querySelector(`#budg-${budgetId}`);
                budgetsContainer.removeChild(budget);
            }
        )
    })
}


function createBudget(budget) {
    const template = document.querySelector("#budget-template");
    const clone = template.content.cloneNode(true);

    const budg = clone.querySelector(".budg");
    const circleIcon = clone.querySelector('.circle i');
    const h2 = clone.querySelector('h2');
    const circle = clone.querySelector('.circle');
    const dates = clone.querySelectorAll('.budg-dates p');
    const values = clone.querySelectorAll('.budg-walet span');
    const progressBar = clone.querySelector('.progress-bar');
    const deleteButton = clone.querySelector('.red-square');

    budg.id = `budg-${budget.budgetId}`;

    h2.innerHTML = budget.name;
    circle.style.background = budget.color;
    circleIcon.classList.add(budget.icon);

    dates[0].innerHTML = `<span>From:</span> ${budget.startDate}`;
    dates[1].innerHTML = `<span>To:</span> ${budget.endDate}`;

    calculateBudgetValues(
        parseFloat(budget.value),
        parseFloat(budget.amountSpent),
        values,
        progressBar
    )

    addDeleteListener(deleteButton, budget.budgetId);

    budgetsContainer.appendChild(clone);
}

function calculateBudgetValues(value,amountSpent,values,progressBar){
    values[0].innerHTML = value.toFixed(2) + 'zł';

    if (amountSpent === null || amountSpent > 0) {
        values[1].innerHTML = "Left " + value.toFixed(2) + 'zł';

        progressBar.style = `--progress: 0%`;
        progressBar.firstElementChild.innerHTML =`0%`;
    }
    else if (amountSpent > -value) {
        let amountLeft = Math.abs(value + amountSpent);
        values[1].innerHTML = "Left " + amountLeft.toFixed(2) + 'zł';

        let progress = amountSpent/(-value) *100;
        progressBar.style = `--progress: ${progress}%`;
        progress = progress.toFixed(2);
        progressBar.firstElementChild.innerHTML =`${progress}%`;
    }
    else {
        let amountOver = Math.abs(amountSpent + value);
        values[1].innerHTML = "Over " + amountOver.toFixed(2) + 'zł';

        progressBar.style = `--progress: 100%`;
        progressBar.firstElementChild.innerHTML =`100%`;
        progressBar.classList.add("progress-bar-full");
    }
}

function validateForm() {
    const a = inputs[0].value;
    const b = inputs[1].value;
    const c = inputs[2].value;
    if (a === null || a === "" || b === null || b === "" || c === null || c === "") {
        alert("Please Fill All Required Field");
        return false;
    }

    if (b > c) {
        alert("Start date can't be greater than end date");
        return false;
    }

    return true;
}

function setTwoNumberDecimal() {
    this.value = Math.abs(parseFloat(this.value)).toFixed(2);
}


const addWindowButton = document.querySelector('.add-window-btn');
const addWindow = document.querySelector('.add-window');
const nav = document.querySelector('nav');
const section = document.querySelector('section');
const header = document.querySelector('#main-header');
const closedIcon = document.querySelector('.closed-icon');
const selectContainer = document.querySelector(".select_wrap");

addWindowButton.addEventListener('click', () => {
    openAddWindow();
})

closedIcon.addEventListener('click', () => {
    closeAddWindow()
    clearForm();
})

function openAddWindow() {
    addWindow.classList.add('add-window-open');
    nav.classList.add('blur');
    section.classList.add('blur');
    header.classList.add('blur');
}

function closeAddWindow() {
    addWindow.classList.remove('add-window-open');
    nav.classList.remove('blur');
    section.classList.remove('blur');
    header.classList.remove('blur');
    selectContainer.classList.remove('active');
}

function clearForm() {
    inputs.forEach((input) => {
        input.value = '';
    })
}