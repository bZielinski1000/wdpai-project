function fetchWrapper(
    url,
    data,
    method,
    resultFunction = () => {
    },
    responseFunction = response => {
        return response.json()
    }) {
    let init;
    if (method === 'GET') {
        init = {
            method: method,
            headers: {
                'Content-Type': 'application/json'
            }
        }
    } else {
        init = {
            method: method,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
    }


    fetch(url, init)
        .then((response) => {
            return responseFunction(response);
        })
        .then((json) => {
            resultFunction(json);
        })
}